package com.mindex.challenge.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.data.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompensationServiceImplTest
{

	private String createUrl;
	private String readUrl;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Before
	public void setup() {
		createUrl = "http://localhost:" + port + "/compensation";
		readUrl = "http://localhost:" + port + "/compensation/{id}";
	}

	@Test
	public void testCreateRead() {
		Compensation testComp = new Compensation();
		testComp.setEffectiveDate(LocalDate.of(2019, Month.JULY, 8).toString()); // Birthday!
		testComp.setSalary(42);
		Employee employee = new Employee();
		employee.setEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f");
		testComp.setEmployee(employee);

		// Create checks
		Compensation createdComp = restTemplate.postForEntity(createUrl, testComp, Compensation.class)
				.getBody();
		assertNotNull(createdComp.getEmployee());
		assertNotNull(createdComp.getEffectiveDate());
		assertNotNull(createdComp.getSalary());
		assertCompensationEquivalence(testComp, createdComp);

		// Read checks
		Compensation readComp = restTemplate
				.getForEntity(readUrl, Compensation.class, createdComp.getEmployee().getEmployeeId())
				.getBody();
		assertNotNull(readComp.getEmployee());
		assertNotNull(readComp.getEffectiveDate());
		assertNotNull(readComp.getSalary());
		assertCompensationEquivalence(createdComp, readComp);
	}

	private static void assertCompensationEquivalence(Compensation expected, Compensation actual) {
		assertEquals(expected.getEmployee().getEmployeeId(), actual.getEmployee().getEmployeeId());
		assertEquals(expected.getEffectiveDate(), actual.getEffectiveDate());
		assertEquals(expected.getSalary(), actual.getSalary());
	}
}
