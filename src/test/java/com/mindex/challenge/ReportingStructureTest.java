package com.mindex.challenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportingStructureTest
{
	private String reportUrl;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Before
	public void setup() {
		reportUrl = "http://localhost:" + port + "/reports/{id}";
	}

	@Test
	public void testReport() {
		String testEmployeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f";

		ReportingStructure report = restTemplate
				.getForEntity(reportUrl, ReportingStructure.class, testEmployeeId).getBody();
		Employee employee = report.getEmployee();

		assertNotNull(employee);
		assertEquals(employee.getEmployeeId(), testEmployeeId);
		assertEquals(report.getNumberOfReports(), 4);
	}
}
