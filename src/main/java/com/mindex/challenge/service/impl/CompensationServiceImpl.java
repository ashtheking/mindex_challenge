package com.mindex.challenge.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindex.challenge.dao.CompensationRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.service.CompensationService;
import com.mindex.challenge.service.EmployeeService;

@Service
public class CompensationServiceImpl implements CompensationService
{
	private static final Logger LOG = LoggerFactory.getLogger(CompensationServiceImpl.class);

	@Autowired
	private CompensationRepository compRepository;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public Compensation create(Compensation comp) {
		LOG.debug("Creating compensation entry [{}]", comp);

		return compRepository.insert(comp);
	}

	@Override
	public Compensation read(String employeeId) {
		LOG.debug("Finding compensation entry for employee id [{}]", employeeId);

		Compensation comp = compRepository.findByEmployeeEmployeeId(employeeId);
		if (comp == null)
			throw new RuntimeException("Invalid employee id: " + employeeId);

		comp.setEmployee(employeeService.read(employeeId));
		return comp;
	}

}
