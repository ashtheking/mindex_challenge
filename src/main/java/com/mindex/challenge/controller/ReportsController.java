package com.mindex.challenge.controller;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.EmployeeService;

@RestController
public class ReportsController
{
	private static final Logger LOG = LoggerFactory.getLogger(ReportsController.class);

	// We need access to the employee service to look up employees
	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/reports/{id}")
	public ReportingStructure read(@PathVariable String id) {
		LOG.debug("Received reporting structure get request for id [{}]", id);

		// This has the possibility of throwing a RuntimeException in practice.
		// However, that's not known or documented for the read() method,
		// So we cannot act here to catch it and handle it. The error itself
		// is if the endpoint is hit with a bad id, which is outside our scope.
		Employee e = employeeService.read(id);
		// If it's null then return null
		if (e == null)
			return null;
		// Create new reporting structure paired to this employee
		ReportingStructure report = new ReportingStructure();
		report.setEmployee(e);
		// Build number of reports, iteratively
		Queue<Employee> head = new LinkedList<>();
		Set<String> reached = new HashSet<>();
		head.add(e); // Add the starter
		while (!head.isEmpty()) {
			// Pop the head of the queue
			e = head.poll();
			// Do a lookup based on id because getDirectReports() doesn't fill
			// The array with full references so we have to ourselves.
			String eId = e.getEmployeeId();
			// Same potential errors ehre.
			e = employeeService.read(eId);
			// Add it to the set
			reached.add(eId);
			// Go through "children"
			if (e.getDirectReports() != null)
				for (Employee emp : e.getDirectReports())
					if (!reached.contains(emp.getEmployeeId()))
						head.add(emp);
		}
		// We add the starter to reached so we do -1 to ignore it
		report.setNumberOfReports(reached.size() - 1);

		return report;
	}
}
