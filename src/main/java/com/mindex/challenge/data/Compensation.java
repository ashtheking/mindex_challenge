package com.mindex.challenge.data;

public class Compensation
{

	private Employee employee;
	private Number salary;
	private String effectiveDate;

	public Compensation()
	{
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Number getSalary() {
		return salary;
	}

	public void setSalary(Number salary) {
		this.salary = salary;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

}
